#Conky

 **THIS PULLS FROM GIT**


[**Conky**][Conky] is a free, light-weight system monitor for X, that displays
any kind of information on your desktop.

## Features

Conky can display more than 300 built-in objects, including support for:

 * A plethora of OS stats (uname, uptime, **CPU usage**, **mem
   usage**, disk usage, **"top"** like process stats, and **network
   monitoring**, just to name a few).
 * Built-in **IMAP** and **POP3** support.
 * Built-in support for many popular music players ([MPD][],
   [XMMS2][], [BMPx][], [Audacious][]).
 * Can be extended using built-in [**Lua**] support, or any of your
   own scripts and programs more.
 * Built-in [**Imlib2**][Imlib2] and [**Cairo**][cairo] bindings for arbitrary drawing
   with Lua more.

... and much much more.

Conky can display information either as text, or using simple progress
bars and graph widgets, with different fonts and colours.

## Options
**AUDACIOUS=yes** (Requires libaudclient)

**NVIDIA=yes**    (Requires Nvidia Drivers, libxnvctrl)

## Dependencies
lua

imlib2

tolua++


[MPD]: http://musicpd.org/
[XMMS2]: http://wiki.xmms2.xmms.se/index.php/Main_Page
[BMPx]: http://bmpx.backtrace.info/site/BMPx_Homepage
[Audacious]: http://audacious-media-player.org/
[luawiki]: http://en.wikipedia.org/wiki/Lua_%28programming_language%29
[Imlib2]: http://docs.enlightenment.org/api/imlib2/html/
[cairo]: http://www.cairographics.org/
[Conky]: https://github.com/brndnmtthws/conky
